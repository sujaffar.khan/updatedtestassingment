package com.creamson.myelsaadminportal

import com.creamson.myelsaadminportal.support.BuildConfig
import com.creamson.myelsaadminportal.support.NetworkService
import io.reactivex.Observable
import retrofit2.http.GET
import java.net.NetworkInterface
import java.util.*

interface DataApi {
    companion object : NetworkService<DataApi>(BuildConfig.BASE_API_URL1, DataApi::class.java)

    @GET("s/2iodh4vg0eortkl/facts.json")
    fun getApiData():Observable<Data.Response>
}