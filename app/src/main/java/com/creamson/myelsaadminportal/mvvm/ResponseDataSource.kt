package com.creamson.myelsaadminportal.mvvm

import com.creamson.myelsaadminportal.Data
import com.creamson.myelsaadminportal.entities.Outcome
import com.creamson.myelsaadminportal.schedulers.BaseSchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

interface ResponseDataSource {
    val scheduler: BaseSchedulerProvider
    val DataResponse: PublishSubject<Outcome<Data.Response>>
    fun getDataResponse(disposable: CompositeDisposable)
}