package com.creamson.myelsaadminportal.mvvm

import androidx.lifecycle.LiveData
import com.creamson.myelsaadminportal.Data
import com.creamson.myelsaadminportal.base.vm.BaseViewModel
import com.creamson.myelsaadminportal.entities.Outcome
import com.creamson.myelsaadminportal.ext.toLiveData

class DataViewModel : BaseViewModel(){
private val dataRepo = DataRepository.getInstance()
    val allDataOutcome: LiveData<Outcome<Data.Response>> by lazy {
        dataRepo.DataResponse.toLiveData(getCompositeDisposal())
    }

    fun getAllData(){
        dataRepo.getDataResponse(getCompositeDisposal())
    }

}