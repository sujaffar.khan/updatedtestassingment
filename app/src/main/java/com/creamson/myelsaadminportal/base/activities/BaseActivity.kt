package com.creamson.myelsaadminportal.base.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.creamson.myelsaadminportal.R
import org.jetbrains.anko.find

abstract class BaseActivity : AppCompatActivity() {

    @LayoutRes
    abstract fun getLayout(): Int

    @MenuRes
    abstract fun getMenu(): Int

    private var mToolbar: Toolbar? = null

    protected fun getToolbar() = mToolbar

    protected fun hideHeaderAppIcon() {
       find<ImageView>(R.id.toolbar_app_icon)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayout())
        mToolbar = findViewById(R.id.toolbar)
        if(mToolbar!=null){
           setSupportActionBar(mToolbar)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
        bindViews(savedInstanceState)
    }

    abstract fun bindViews(savedInstanceState: Bundle?)

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        return if(hasMenu()){
            menuInflater.inflate(getMenu(), menu)
            true
        } else super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun hasMenu() = getMenu() != 0
}