package com.creamson.myelsaadminportal.base.vm

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

open class  BaseViewModel : ViewModel(){
    private val compositeDisposable = CompositeDisposable()

    val dataLoading = MutableLiveData<Boolean>()

    val dataMessage = MutableLiveData<Int>()

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    protected fun getCompositeDisposal() = compositeDisposable

    protected fun isProcessing(isLoading: Boolean){
        dataLoading.value = isLoading
    }

    protected fun setMessage(@StringRes messageResInt: Int){
        dataMessage.value = messageResInt
    }
}