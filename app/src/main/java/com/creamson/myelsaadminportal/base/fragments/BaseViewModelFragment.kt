package com.creamson.myelsaadminportal.base.fragments

import android.os.Bundle
import androidx.lifecycle.Observer
import com.creamson.myelsaadminportal.base.vm.BaseViewModel
import com.creamson.myelsaadminportal.ext.reObserve

abstract class BaseViewModelFragment<V : BaseViewModel> : BaseFragment(){
    private val mLoadingObserver = Observer<Boolean> { isProcessing(it) }

    private val mMessageObserver = Observer<Int> { showMessage(it) }

    protected lateinit var mViewModel: V

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mViewModel.dataMessage.reObserve(this, mMessageObserver)
        mViewModel.dataLoading.reObserve(this, mLoadingObserver)
    }

    abstract fun isProcessing(isLoading: Boolean)

    abstract fun showMessage(messageResId: Int)
}