package com.creamson.myelsaadminportal.base.fragments

import android.os.Bundle
import android.view.*
import androidx.annotation.LayoutRes
import androidx.annotation.MenuRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

import com.creamson.myelsaadminportal.ext.text

abstract class BaseFragment : Fragment(){
    @LayoutRes
    abstract fun getLayout(): Int

    @MenuRes
    abstract fun getMenu(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(hasMenu())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(getLayout(), container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        if (hasMenu()) inflater?.inflate(getMenu(), menu)
        else super.onCreateOptionsMenu(menu, inflater)
    }

    private fun hasMenu() = getMenu() != 0

    protected fun getBaseActivity() = activity as AppCompatActivity

    protected fun onBackPressed() {
        activity?.onBackPressed()
    }

    protected fun setTitle(@StringRes titleResId: Int) {
        setTitle(text(titleResId))
    }

    protected fun setTitle(title: String) {
        (activity as AppCompatActivity).supportActionBar?.title = title
    }

    protected fun setIcon(iconResId: Int) {
        (activity as AppCompatActivity).supportActionBar?.setHomeAsUpIndicator(iconResId)
    }
}